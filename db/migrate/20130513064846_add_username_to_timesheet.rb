class AddUsernameToTimesheet < ActiveRecord::Migration
  def change
    add_column :timesheets, :username, :string
  end
end
