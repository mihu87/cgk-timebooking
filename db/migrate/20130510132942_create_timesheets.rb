class CreateTimesheets < ActiveRecord::Migration
  def change
    create_table :timesheets do |t|
      t.date :date
      t.string :time
      t.string :project
      t.string :issue
      t.text :comment

      t.timestamps
    end
  end
end
