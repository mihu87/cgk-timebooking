# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Account.create([{ username: 'petrep', password: '1aecc21e1472a306762ee07069b9c691ecc37fd3' }])
Account.create([{ username: 'mihaie', password: '1e9f62d68fb64af91ddfa2b2d8f0e8913a4f4853' }])
Account.create([{ username: 'mariusa', password: 'b1ac0cfbe3247f603001fcb0a45a9f99f5613e36' }])
Account.create([{ username: 'ionutm', password: '14c8b5bbc4b586ccfb2608f773dab2cdb6e70433' }])
Account.create([{ username: 'amatthij', password: '36ecc92208f30789ad0e952d74cabc0fd71f9e74'}])
