class Timesheet < ActiveRecord::Base
  attr_accessible :comment, :date, :issue, :project, :time

  # Gets the user timesheet data and orders in accordingly
  def self.get_user_data (username)
	return Timesheet.where(:username => username).order("date DESC")
  end

  def self.get_report_data (username)
  	return Timesheet.where(:username => username).order("project DESC").order("date DESC")
  end

  # Gets the dates available for reporting
  def self.get_user_report_dates (username) 
  	return Timesheet.where(:username => username).order("project DESC").order("date DESC").select([:date])
  end
end
