module ApplicationHelper
	# Transform achievo code to project name.
	def achievo_code_to_project_name code
		case code
		when 'P01185'
			return 'Marketing'
		when 'P01193'
			return 'Ordertool'
		when 'P01285'
			return 'Smartspot'
		when 'P01299'
			return 'Realspeed'
		when 'P01209'
			return "BRE On Line"
		when '1060558'
			return "SEPA DD"
		when 'P01305'
			return "Popcorn"
		when 'Competence Centre'
			return 'Competence Centre'
		when 'Sun Phase 3'
			return 'Sun Phase 3'
		when 'Testing'
			return 'Testing'
		when 'Refactorings'
			return 'Refactorings'
		end
	end

	def achievo_code_to_camis_wo code
		case code
		when 'P01185'
			return 'TLN-000.100'
		when 'P01193'
			return 'TLN-000.100'
		when 'P01285'
			return 'TLN-000.100'
		when 'P01299'
			return 'TLN-000.100'
		when 'P01209'
			return 'TLN-000.100'
		when '1060558'
			return 'TLN-000.100'
		when 'P01305'
			return 'TLN-000.100'
		when 'Competence Centre'
			return 'OPLE914.050'
		when 'Sun Phase 3'
			return 'TLN-001.030'
		else
			return 'TLN-000.001'
		end
	end

	def achievo_code_to_achievo_fulltext code
		case code
		when 'P01185'
			return 'P01185: 2013-Product Marketing Mobile'
		when 'P01193'
			return 'P01193: 2013-Operational Development'
		when 'P01285'
			return 'P01285: 2013-IT_RSC - Smartspot Home Delivery'
		when 'P01299'
			return 'P01299: 2013-IT_RMD - NTL Realspeed'
		when 'P01209'
			return 'P01209: 2013-BRE On Line'
		when '1060558'
			return '1060558: 2013-IT SEPA direct debit-1060558'
		when 'P01305'
			return 'P01305: 2013-IT_RMD - Popcorn'
		else
			return 'Unbookable in Achievo'
		end
	end

	def concat_projects entries
		output = []

		entries.each do |e|
			output.push(e.project)
		end

		return output.join(' + ')
	end

	def concat_time entries
		hours = 0
		minutes = 0

		entries.each do |e|
			splitted = e.time.split(/:/)

			hours += splitted.first.to_i
			minutes += splitted.second.to_i
		end

		if minutes <= 60
			hours += minutes / 60
			minutes = minutes % 60
		end

		return "#{hours}:#{minutes}"
	end

	def concat_work_orders entries
		output = []

		entries.each do |e|
			output.push(achievo_code_to_camis_wo e.project)
		end

		return output.join(' + ')
	end

	def concat_issues entries
		output = []

		entries.each do |e|
			output.push(e.issue)
		end

		return output.join(' + ')
	end

	def date_performed_camis date
		date = date.to_time.strftime('%a/%m/%d')
	end

	def date_performed_achievo date
		date = date.to_time.strftime('%A %d/%B/%Y')
	end

	def date_started_jira date
		date = date.to_time.strftime('%d/%B/%y')

		return "#{date} 12:00 PM"
	end

	def link_to_jira issue
		link_to issue, "http://jira.tln-dev.cegeka.be/jira/browse/#{issue}", :target => "_blank"
	end

	def time_to_jira time
		time = time.split(/:/)
		hours = time.first.to_i
		minutes = time.second.to_i

		if minutes >= 60
			hours += minutes / 60
			minutes = minutes % 60
		end

		output = "#{hours}h #{minutes}m"
	end

	def time_to_camis time
		time = time.split(/:/)
		hours = time.first.to_i
		minutes = time.second.to_i

		if minutes <= 60
			hours += minutes / 60
			minutes = minutes % 60
		end

		if minutes < 10
			minutes = "#{minutes}0"
		end

		return "#{hours}.#{minutes}"
	end
end


# ['Marketing', 'P01185'], 
# ['Ordertool', 'P01193'],
# ['Smartspot', 'P01285'],
# ['Realspeed', 'P01299']
