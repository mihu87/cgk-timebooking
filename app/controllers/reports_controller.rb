class ReportsController < ApplicationController
  def generate
  	# Get parameter
  	@report_date = params[:timesheet][:report_date]
  	logger.debug @report_date

  	# Reference data
  	@timesheets = Timesheet.where(:username => session[:username], :date => @report_date)

  	# Feed data to view
  	if @timesheets.empty?
  		flash[:notice] = "There is no data available on the selected date."
  	end
  end
end
