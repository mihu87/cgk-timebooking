class HomeController < ApplicationController
  def index
  	if session[:logged_in].nil?
  		logger.debug "Not logged in"

  		redirect_to login_path
  	else
  		logger.debug "Logged in"

  		@timesheets = Timesheet.get_user_data session[:username]
  		@report_dates = Timesheet.get_user_report_dates session[:username]
  	end
  end
end
