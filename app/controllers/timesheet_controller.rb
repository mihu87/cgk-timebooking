class TimesheetController < ApplicationController
  def add
  	if request.post?
  		# Get parameters
  		date = Date.parse(params[:timesheet_date].to_a.sort.collect{|c| c[1]}.join("-"))

  		# Get time
  		hours = params[:timesheet_time][:'work_time(4i)']
  		minutes = params[:timesheet_time][:'work_time(5i)']
  		time = "#{hours}:#{minutes}"

  		project = params[:project]
  		issue = params[:issue]
  		comment = params[:comment]

  		logger.debug time

  		# Create new timesheet entry
  		timesheet = Timesheet.new
  		timesheet.date = date
  		timesheet.time = time
  		timesheet.project = project
  		timesheet.issue = issue
  		timesheet.comment = comment
  		timesheet.username = session[:username]
  		timesheet.save

  		redirect_to home_path
  	end
  end
end
