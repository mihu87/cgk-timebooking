class AccountController < ApplicationController
  def login
  	if request.post?
  		logger.debug "Login form POST request"

  		username = params[:username]
  		password = params[:password]

  		# Encode password
  		password = Digest::SHA1.hexdigest(password)

  		# Get user account
  		account = Account.find_by_username_and_password(username, password)

  		if account.blank?
  			logger.debug "Login details incorrect for user [#{username}]"
  			flash[:error] = "Login details are incorrect."
  		else
  			logger.debug "Login details are correct for user [#{username}]"
  			flash[:notice] = "You have successfully logged in."

  			# Set session data
  			session[:logged_in] = true
  			session[:username] = username

  			redirect_to home_path
  		end
  	end
  end

  def logout
  	reset_session
  end
end
